package simple

import org.scalatest.{FreeSpec, Matchers}
import simple.lists.{WorkWithListRecImpl, WorkWithListImpl}

class SimpleTestSpec extends FreeSpec with Matchers {
  //val impl = simple.impl.WorkWithListImpl
  val impl = WorkWithListRecImpl

  "Working with lists" - {
    import impl._
    "P01 (*)Find the last element of a list." in {
      val result = last(List(1, 1, 2, 3, 5, 8))
      result shouldBe 8

      an [NoSuchElementException] should be thrownBy {
        last(List())
      }
    }
    
    "P02 (*) Find the last but one element of a list." in {
      val result =  penultimate(List(1, 1, 2, 3, 5, 8))
      result shouldBe 5

      an [RuntimeException] should be thrownBy {
        penultimate(List())
      }

      an [NoSuchElementException] should be thrownBy {
        penultimate(List(1))
      }

      penultimate(List(1, 2)) shouldBe 1
    }
    "P03 (*) Find the Kth element of a list." in {
      val result =  nth(2, List(1, 1, 2, 3, 5, 8))
      result shouldBe 2
    }
    "P04 (*) Find the number of elements of a list." in {
      val result =  impl.length(List(1, 1, 2, 3, 5, 8))
      result shouldBe 6
    }
    "P05 (*) Reverse a list." in {
      val result =  reverse(List(1, 1, 2, 3, 5, 8))
      result shouldBe List(8, 5, 3, 2, 1, 1)
    }
    "P06 (*) Find out whether a list is a palindrome." in {
      val result =  isPalindrome(List(1, 2, 3, 2, 1))
      result shouldBe true

    }
    "P07 (**) Flatten a nested list structure." in {
      val result =  flatten(List(List(1, 1), 2, List(3, List(5, 8))))
      result shouldBe List(1, 1, 2, 3, 5, 8)
    }
    "P08 (**) Eliminate consecutive duplicates of list elements." in {
      val result =  compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
      result shouldBe List('a, 'b, 'c, 'a, 'd, 'e)
    }
    "P09 (**) Pack consecutive duplicates of list elements into sublists." in {
      val result =  pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
      result shouldBe List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e))
    }
    "P10 (*) Run-length encoding of a list." in {
      val result =  encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
      result shouldBe List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e))
    }
    "P11 (*) Modified run-length encoding." in {
      val result =  encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
      result shouldBe List((4,'a), 'b, (2,'c), (2,'a), 'd, (4,'e))
    }
    "P12 (**) Decode a run-length encoded list." in {
      val result =  decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e)))
      result shouldBe List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
    }
    "P13 (**) Run-length encoding of a list (direct solution)." in {
      val result =  encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
      result shouldBe List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e))
    }
    " P14 (*) Duplicate the elements of a list." in {
      val result =  duplicate(List('a, 'b, 'c, 'c, 'd))
      result shouldBe List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd)
    }
    " P15 (**) Duplicate the elements of a list a given number of times." in {
      val result =  duplicateN(3, List('a, 'b, 'c, 'c, 'd))
      result shouldBe List('a, 'a, 'a, 'b, 'b, 'b, 'c, 'c, 'c, 'c, 'c, 'c, 'd, 'd, 'd)
    }
    " P16 (**) Drop every Nth element from a list." in {
      val result =  drop(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
      result shouldBe List('a, 'b, 'd, 'e, 'g, 'h, 'j, 'k)
    }
    " P17 (*) Split a list into two parts." in {
      val result =  split(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
      result shouldBe (List('a, 'b, 'c),List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
    }
    " P18 (**) Extract a slice from a list." in {
      val result =  slice(3, 7, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
      result shouldBe List('d, 'e, 'f, 'g)
    }
    " P19 (**) Rotate a list N places to the left." in {
      val result1 =  rotate(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
      result1 shouldBe List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'a, 'b, 'c)

      val result2 =  rotate(-2, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
      result1 shouldBe List('j, 'k, 'a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i)
    }
    " P20 (*) Remove the Kth element from a list." in {
      val result =  removeAt(1, List('a, 'b, 'c, 'd))
      result shouldBe (List('a, 'c, 'd),'b)

    }
    " P21 (*) Insert an element at a given position into a list." in {
      val result =  insertAt('new, 1, List('a, 'b, 'c, 'd))
      result shouldBe List('a, 'new, 'b, 'c, 'd)
    }
    " P22 (*) Create a list containing all integers within a given range." in {
      val result =  range(4, 9)
      result shouldBe List(4, 5, 6, 7, 8, 9)
    }
    " P23 (**) Extract a given number of randomly selected elements from a list." in {
      val result =  randomSelect(3, List('a, 'b, 'c, 'd, 'f, 'g, 'h))
      result shouldBe List('e, 'd, 'a)
    }
    " P24 (*) Lotto: Draw N different random numbers from the set 1..M." in {
      val result =  lotto(6, 49)
      result shouldBe List(23, 1, 17, 33, 21, 37)
    }
    " P25 (*) Generate a random permutation of the elements of a list." in {
      val result =  randomPermute(List('a, 'b, 'c, 'd, 'e, 'f))
      result shouldBe List('b, 'a, 'd, 'c, 'e, 'f)
    }
    " P26 (**) Generate the combinations of K distinct objects chosen from the N elements of a list." in {
      val result =  combinations(3, List('a, 'b, 'c, 'd, 'e, 'f))
      result should contain(List('a, 'b, 'c))
      result should contain(List('a, 'b, 'd))
      result should contain(List('a, 'b, 'e))
    }
    " P27 (**) Group the elements of a set into disjoint subsets." in {
      val result =  group3(List("Aldo", "Beat", "Carla", "David", "Evi", "Flip", "Gary", "Hugo", "Ida"))
      result should contain(List(List("Aldo", "Beat"), List("Carla", "David", "Evi"), List("Flip", "Gary", "Hugo", "Ida")))
    }
    " P28 (**) Sorting a list of lists according to length of sublists." in {
      val result1 =  lsort(List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o)))
      result1 shouldBe List(List('o), List('d, 'e), List('d, 'e), List('m, 'n), List('a, 'b, 'c), List('f, 'g, 'h), List('i, 'j, 'k, 'l))

      val result2 =  lsortFreq(List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o)))
      result2 shouldBe List(List('i, 'j, 'k, 'l), List('o), List('a, 'b, 'c), List('f, 'g, 'h), List('d, 'e), List('d, 'e), List('m, 'n))


    }
  }
}