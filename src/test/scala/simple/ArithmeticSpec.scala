package simple

import org.scalatest.{Matchers, FreeSpec}

/**
 * Created by alexgri on 27.10.14.
 */
class ArithmeticSpec extends FreeSpec with Matchers {
  "Arithmetic" - {
    import simple.impl.ArithmeticImpl._
    "P31 (**) Determine whether a given integer number is prime." in {

      val result =  7.isPrime
      result shouldBe true

    }

    "P32 (**) Determine the greatest common divisor of two positive integer numbers." in {
      val result =  gcd(36, 63)
      result shouldBe 9

    }


    "P33 (*) Determine whether two positive integer numbers are coprime." in {
      val result = 35.isCoprimeTo(64)
      result shouldBe true

    }


    "P34 (**) Calculate Euler's totient function phi(m)." in {
      val result = 10.totient
      result shouldBe 4

    }


    "P35 (**) Determine the prime factors of a given positive integer." in {
        val result = 315.primeFactors
      result shouldBe List(3, 3, 5, 7)

    }


    "P36 (**) Determine the prime factors of a given positive integer (2)." in {
      val result = 315.primeFactorMultiplicity
      result shouldBe List((3,2), (5,1), (7,1))

      //Alternately, use a Map for the result.
      val result2 = 315.primeFactorMultiplicityMap
      result2 shouldBe Map(3 -> 2, 5 -> 1, 7 -> 1)
    }




    "P37 (**) Calculate Euler's totient function phi(m) (improved)."
    "P38 (*) Compare the two methods of calculating Euler's totient function."
    "P39 (*) A list of prime numbers." in {
      val result = listPrimesinRange(7 to 31)
      result shouldBe List(7, 11, 13, 17, 19, 23, 29, 31)

    }

    "P40 (**) Goldbach's conjecture." in {
        val result = 28.goldbach
      result shouldBe (5,23)

    }

    "P41 (**) A list of Goldbach compositions." in {
      val result1 = printGoldbachList(9 to 20)
      result1 should contain("10 = 3 + 7")
      result1 should contain("12 = 5 + 7")
      result1 should contain("14 = 3 + 11")
      result1 should contain("16 = 3 + 13")
      result1 should contain("18 = 5 + 13")
      result1 should contain("20 = 3 + 17")

      val result2 = printGoldbachListLimited(1 to 2000, 50)
      result2 should contain ("992 = 73 + 919")
      result2 should contain ("1382 = 61 + 1321")
      result2 should contain ("1856 = 67 + 1789")
      result2 should contain ("1928 = 61 + 1867")
    }
  }
}
