package simple.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Arithmetic {
  def gcd(a:Int, b:Int):Int
  def listPrimesinRange(r:Range):List[Int]
  def printGoldbachList(r:Range):List[String]
  def printGoldbachListLimited(r:Range, limit: Int):List[String]
}
