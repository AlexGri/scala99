package simple.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait ArithmeticRichInt {
  def isPrime:Boolean
  def isCoprimeTo(i:Int):Boolean
  def totient:Int
  def primeFactors:List[Int]
  def primeFactorMultiplicity:List[(Int, Int)]
  def primeFactorMultiplicityMap:Map[Int, Int]
  def goldbach:(Int, Int)
}
