package simple.impl

import simple.traits.{ArithmeticRichInt, Arithmetic}

/**
 * Created by alexgri on 27.10.14.
 */
object ArithmeticImpl extends Arithmetic {
  override def gcd(a: Int, b: Int): Int = ???

  override def printGoldbachListLimited(r: Range, limit: Int): List[String] = ???

  override def printGoldbachList(r: Range): List[String] = ???

  override def listPrimesinRange(r: Range): List[Int] = ???

  implicit class S99Int(val start: Int) extends ArithmeticRichInt {
    override def isPrime: Boolean = ???

    override def goldbach: (Int, Int) = ???

    override def primeFactorMultiplicity: List[(Int, Int)] = ???

    override def isCoprimeTo(i:Int): Boolean = ???

    override def primeFactors: List[Int] = ???

    override def primeFactorMultiplicityMap: Map[Int, Int] = ???

    override def totient: Int = ???
  }
}
