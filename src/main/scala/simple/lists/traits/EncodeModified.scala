package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait EncodeModified {
  def encodeModified(l:List[_]):List[_]
}
