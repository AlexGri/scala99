package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Pack {
  def pack[A](l:List[A]):List[List[A]]
}
