package simple.lists.traits

trait WorkWithList {
  self: Last with Penultimate with Nth with Length with Reverse
   with IsPalindrome with Flatten with Compress with Pack with Encode
   with EncodeModified with Split with Drop with DuplicateN with EncodeDirect
    with Duplicate with Decode with  Slice with Rotate with RemoveAt
    with InsertAt with Range with RandomSelect with Lotto with RandomPermute
    with Combinations with Group3 with Group with Lsort with LsortFreq =>
}

trait Split { def split[A](n:Int, l:List[A]):(List[A], List[A]) }
trait Drop { def drop[A](n:Int, l:List[A]):List[A] }
trait DuplicateN { def duplicateN[A](n:Int, l:List[A]):List[A] }
trait EncodeDirect { def encodeDirect[A](l:List[A]):List[(Int, A)] }
trait Duplicate { def duplicate[A](l:List[A]):List[A] }
trait Slice {def slice[A](i:Int, k:Int, l:List[A]):List[A]}
trait Rotate {def rotate[A](n:Int, l:List[A]):List[A]}
trait RemoveAt {def removeAt[A](n:Int, l:List[A]):(List[A], A)}
trait InsertAt {def insertAt[A](el:A, n:Int, l:List[A]):List[A]}
trait Range {def range(s:Int, e:Int):List[Int]}
trait RandomSelect {def randomSelect[A](n:Int, l:List[A]):List[A]}
trait Lotto {def lotto(n:Int , m:Int):List[Int]}
trait RandomPermute {def randomPermute[A](l:List[A]):List[A]}
trait Combinations {def combinations[A](n:Int, l:List[A]):List[List[A]]}
trait Group3 {def group3(l:List[String]):List[List[List[String]]]}
trait Group {def group(settings:List[Int], in:List[String]):List[List[List[String]]]}
trait Lsort {def lsort[A](l:List[List[A]]):List[List[A]]}
trait LsortFreq {def lsortFreq[A](l:List[List[A]]):List[List[A]]}