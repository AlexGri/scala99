package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Length {
  def length(l:List[_]):Int
}
