package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Reverse {
  def reverse[A](l:List[A]):List[A]
}
