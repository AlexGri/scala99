package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Nth {
  def nth[A](n:Int, l: List[A]):A
}
