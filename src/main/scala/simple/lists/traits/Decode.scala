package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Decode {
  def decode[A](l:List[(Int, A)]):List[A]
}
