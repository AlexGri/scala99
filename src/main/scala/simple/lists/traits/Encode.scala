package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Encode {
  def encode[A](l:List[A]):List[(Int, A)]
}
