package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Penultimate {
  def penultimate[A](l:List[A]):A
}
