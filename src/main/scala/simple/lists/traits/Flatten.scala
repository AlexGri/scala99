package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Flatten {
  def flatten[A](l: List[_]): List[A]
}
