package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Compress {
  def compress[A](l:List[A]):List[A]
}
