package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait Last {
  def last[A](l:List[A]):A
}
