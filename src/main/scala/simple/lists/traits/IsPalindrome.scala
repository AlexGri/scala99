package simple.lists.traits

/**
 * Created by alexgri on 27.10.14.
 */
trait IsPalindrome {
  def isPalindrome(l:List[Int]):Boolean
}
