package simple.lists.impl

import simple.lists.traits.Reverse

/**
  * Created by alexgri on 27.10.14.
  */
trait ReverseSimpleImpl extends Reverse {
  def reverse[A](l:List[A]):List[A] = ???
}
