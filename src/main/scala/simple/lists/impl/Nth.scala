package simple.lists.impl

import simple.lists.traits.Nth

/**
  * Created by alexgri on 27.10.14.
  */
trait NthSimpleImpl extends Nth {
   def nth[A](n:Int, l: List[A]):A = ???
 }
