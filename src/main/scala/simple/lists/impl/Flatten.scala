package simple.lists.impl

import simple.lists.traits.Flatten

/**
  * Created by alexgri on 27.10.14.
  */
trait FlattenSimpleImpl extends Flatten {
   def flatten[A](l: List[_]): List[A] = ???
 }
