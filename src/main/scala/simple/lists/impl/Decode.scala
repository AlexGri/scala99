package simple.lists.impl

import simple.lists.traits.Decode

/**
  * Created by alexgri on 27.10.14.
  */
trait DecodeSimpleImpl extends Decode {
   def decode[A](l:List[(Int, A)]):List[A] = ???
 }
