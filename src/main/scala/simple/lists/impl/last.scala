package simple.lists.impl

import simple.lists.traits.Last

import scala.annotation.tailrec

/**
 * Created by alexgri on 27.10.14.
 */
trait SimpleLast extends Last {
  override def last[A](l: List[A]): A = l.last
}
trait RecursiveLast extends Last {
  override def last[A](l: List[A]): A = {
    @tailrec
    def innerLast(l: List[A], prev: => A):A = l match {
      case Nil => prev
      case h::tail => innerLast(tail, h)
    }
    innerLast(l, throw new NoSuchElementException)
  }
}
