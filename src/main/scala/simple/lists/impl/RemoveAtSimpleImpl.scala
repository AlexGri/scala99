package simple.lists.impl

import simple.lists.traits.RemoveAt

/**
 * Created by alexgri on 27.10.14.
 */
trait RemoveAtSimpleImpl extends RemoveAt {def removeAt[A](n:Int, l:List[A]):(List[A], A)= ???}
