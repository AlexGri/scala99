package simple.lists.impl

import simple.lists.traits.Penultimate

import scala.annotation.tailrec

/**
 * Created by alexgri on 27.10.14.
 */
trait PenultimateRec extends Penultimate {
  override def penultimate[A](l: List[A]): A = {
    @tailrec
    def inner(l: List[A], prev: => A, prevprev: => A):A = l match {
      case Nil => prevprev
      case h::tail => inner(tail, h, prev)
    }
    inner(l, throw new NoSuchElementException, throw new NoSuchElementException)
  }
}

trait PenultimateSimple extends Penultimate {
  override def penultimate[A](l: List[A]): A = l.init.last
}
