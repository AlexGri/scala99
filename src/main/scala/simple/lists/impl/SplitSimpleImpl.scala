package simple.lists.impl

import simple.lists.traits.Split

/**
 * Created by alexgri on 27.10.14.
 */
trait SplitSimpleImpl extends Split { def split[A](n:Int, l:List[A]):(List[A], List[A]) = ???}
